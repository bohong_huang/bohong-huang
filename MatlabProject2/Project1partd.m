%partd: Write a function that will take matrix argument "A" and vector
%argument "b", and return the solution to Ax = b.
%Define a function, the input are a given matrix and a vector, and the
%output is the solution of Ax = b.
function solution = Project1partd(A,b)
    [m,n] = size(A);
    o = length(b);
    %Get the rank of the matrix A to find the solution
    ra = rank(A);
    %define a array with dimension n to save solution. 
    X = zeros(n,1);
    %define a array to store the index of pivot variables.
    pivots = [];
    %Define a array to store the index of free variables
    freevariables = [];
    %Check if the dimension is matched for A and b.
    if m ~= o
        error("Dimension mismatch")
    end
    %combine matrix A and vector b. 
    B =[A,b];
    RowEchelon = Project1partb(B);
    RowCanonical = Project1partc(RowEchelon);
    R_C_A = RowCanonical(:,1:n);
    R_C_b = RowCanonical(:,n+1);
    %Check if the rank of the Rowcanonical Matrix combine A and b is equal
    %to the original matirx A.
    if ra ~= rank(B)
        error("There is no solution")
    end
    %There is at least one solution, check for unique solution or multiple solution.
    % unique solution, just output it
    if ra == n 
        for i = 1:ra
            for j = 1:n
                if R_C_A(i,j) == 1
                    X(j) = R_C_b(i);
                end
            end
        end
        solution = X;
    end
    
    %then for multiple solution. We need to find the pivot and the free
    %variables.
    if ra < n
        %find the index of pivot variables first
        for i = 1:ra
            for j = 1:n
                if R_C_A(i,j) == 1
                    pivots(end + 1) = j;
                    break
                end
            end
        end
        %then, get the free variable
        for j = 1:n
            if isempty(find(pivots == j))
                freevariables(end + 1) = j;
            end
        end
        %print the output
        for i = 1:length(pivots)
            left =  strcat('x', int2str(pivots(i)));
            right = num2str(R_C_b(i));
            for j = 1:length(freevariables)
                variable = strcat('*x', int2str(freevariables(j)));
                cof = -R_C_A(i, freevariables(j));
                if cof == 0
                    continue
                elseif cof < 0
                    term = strcat(num2str(-cof), variable);
                    right = variable_add(right, term, -1);
                else
                    term = strcat(num2str(cof), variable);
                    right = variable_add(right, term, 1);
                end
            end
            disp(strcat(left, '=',right))
        end
        disp('the variables at the right hand side can take any value')
        solution = "there is multiple solution";
    end        
end